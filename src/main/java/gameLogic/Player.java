package gameLogic;

import utility.KeyboardUtility;

public class Player extends Character {

    int countAbility = 5;

    public Player(String name, int health, int shieldHealth) {
        super(name, health, shieldHealth);
        specialAbilityName = "Heal";
    }

    @Override
    public boolean specialAbility() {
        if (countAbility > 0) {
            health = health + KeyboardUtility.getRandomInt((MAXHEALTH / 2));
            countAbility--;
            return true;
        } else {
            return false;
        }
    }
}
